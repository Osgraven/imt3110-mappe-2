package no.ntnu.imt3110.dypdykkObjectPool;

import java.util.ArrayList;

public class PoolSpawnableObject{
	
	ArrayList<SpawnableObject> pool;
	private int lowerLimit;
	private int upperLimit;
	
	public PoolSpawnableObject(int lowerLimit, int upperLimit){
		this.lowerLimit = lowerLimit;
		this.upperLimit = upperLimit;
		pool = new ArrayList<SpawnableObject>();
		for(int i = 0; i < (upperLimit - lowerLimit)/2; i++){
			pool.add(new SpawnableObject());
		}
	}
	
	private void spawnToPool() {
		pool.add(new SpawnableObject());
	}
	
	public SpawnableObject getObject(){
		SpawnableObject obj = pool.get(0);	//get an object
		pool.remove(0);						//remove it from pool
		if(pool.size() <= lowerLimit)		//check if there are enough in the pool
			spawnToPool();					//if not, spawn new ones
		return obj;							//returns object
	
	}
	public void returnToPool(SpawnableObject obj) {
		if(pool.size() < upperLimit) //only add to the pool if it isnt full.
			pool.add(obj);
	}
}


