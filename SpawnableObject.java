package no.ntnu.imt3110.dypdykkObjectPool;

public class SpawnableObject{
	private int id;
	private String name;
	String[] components;
	
	public SpawnableObject() {
		this.id = 0;
		this.name = "";
		components = new String[100];
		for(int i = 0; i < 100; i++) {
			components[i] = "Component nr " + i; //Simulates the setup of the object
		}
	}
	public SpawnableObject(int id, String name) {
		this.id = id;
		this.name = name;
		components = new String[100];
		for(int i = 0; i < 100; i++) {
			components[i] = "Component nr " + i; //Simulates the setup of the object
		}
	}
	
	public void resetObject(int id, String name) {
		this.id = id;
		this.name = name;
	}
}
