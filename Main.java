package no.ntnu.imt3110.dypdykkObjectPool;

import java.util.ArrayList;

public class Main {
	private static ArrayList<SpawnableObject> objectsInUse;
	public static void main(String[] args) {
		objectsInUse = new ArrayList<SpawnableObject>();
		
		for(int i = 100; i <= 10000; i *= 2) {
			for(int j = 10; j <= 100; j += 10) {
				System.out.printf("%d Objects at %d objects per batch used %.2f ms without object pooling\n", i, j, testNormalSpawning(i, j)/1000000);
				System.out.printf("%d Objects at %d objects per batch usedUsed %.2f ms with object pooling\n\n", i, j, testObjectPooling(i, j)/1000000);
			}
		}	
	}
	
	private static double testNormalSpawning(int objectsToSpawn, int batchSize) {
		long startTime = System.nanoTime();	//gets start time
		for(int i = 0; i < objectsToSpawn; i++) { //for objectsToSpawn
			for(int j = 0; j < batchSize; j++)		//divide into batches at batchSize;
				objectsInUse.add(new SpawnableObject(j, "Object " + j));	//create a new object
			while (objectsInUse.size() > 0)		//while objects are in use
				objectsInUse.remove(0);			//remove the 
		}
		return System.nanoTime() - startTime;	//returns time used
	}
	
	private static double testObjectPooling(int objectsToSpawn, int batchSize) {
		long startTime = System.nanoTime();	//gets start time
		PoolSpawnableObject pool = new PoolSpawnableObject(batchSize/2 , batchSize * 2);	//sets up a pool at a fitting level
		for(int i = 0; i < objectsToSpawn; i++) {	//for each object to spawn
			for(int j = 0; j < batchSize; j++) {	//divide into batches
				SpawnableObject obj = pool.getObject();	//get a object from the pool
				obj.resetObject(j, "Object " + j);		//reset the object to our use
				objectsInUse.add(obj);				//add it to the objectsInUse
			}
			while (objectsInUse.size() > 0) {	//while objects are still in play
				SpawnableObject obj = objectsInUse.get(0);	//get an object from the in use list
				objectsInUse.remove(0);				//remove said object
				pool.returnToPool(obj);				//return it to the pool
			}
		}
		return System.nanoTime() - startTime;	//returns time used
	}
}
